using System.Collections.Generic;
using TokenBasedAuth.Data.DTO;

namespace TokenBasedAuth.Models.Interfaces
{
    public interface IChatRepository
    {
        public List<ChatDTO> GetChatDetails();
     
    }
}