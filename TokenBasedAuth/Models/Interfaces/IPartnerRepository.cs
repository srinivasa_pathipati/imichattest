using System.Collections.Generic;
using TokenBasedAuth.Data.DTO;

namespace TokenBasedAuth.Models.Interfaces
{
    public interface IPartnerRepository
    {
        public List<PartnerDTO> GetPartnerDetails();
        public PartnerDTO GetPartnerDetailsById(int partnerId);
     
    }
}