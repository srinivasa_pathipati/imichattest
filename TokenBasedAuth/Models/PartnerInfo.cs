using System.Collections.Generic;
using TokenBasedAuth.Data.DTO;
using TokenBasedAuth.Models.Interfaces;

namespace TokenBasedAuth.Models
{
    public class PartnerInfo
    {
         private readonly IPartnerRepository _partnerrepository;
        public PartnerInfo(IPartnerRepository partnerrepository)
        {
               _partnerrepository=partnerrepository;
        }
        
        public List<PartnerDTO> GetPartnerDetails()
        {
            //here we use business models not dto models exposed
           return _partnerrepository.GetPartnerDetails();
        }
         public PartnerDTO GetPartnerDetailsById(int partnerid)
        {
            //here we use business models not dto models exposed
           return _partnerrepository.GetPartnerDetailsById(partnerid);
        }
    }
}