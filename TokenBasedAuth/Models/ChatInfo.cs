using TokenBasedAuth.Models.Interfaces;

namespace TokenBasedAuth.Models
{
    public class ChatInfo
    {
         private readonly IChatRepository _chatrepository;
        public ChatInfo(IChatRepository chatrepository)
        {
               _chatrepository=chatrepository;
        }
        
        public void GetChatdetails()
        {
           var result= _chatrepository.GetChatDetails();
        }
    }
}