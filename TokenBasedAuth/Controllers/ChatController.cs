﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Configuration;
using TokenBasedAuth.Models.Interfaces;
using TokenBasedAuth.Models;

namespace TokenBasedAuth.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class ChatController : ControllerBase
    {
      private readonly IConfiguration _configuration;
     
      private readonly ChatInfo _chatinfo;
     public ChatController(IConfiguration configuration,IChatRepository chatRepository)   
     {
       _configuration=configuration;      
       _chatinfo=new ChatInfo(chatRepository);
     }
    
      [HttpGet("GetChats")]
      public IActionResult GetChatDetails()
      {
        _chatinfo.GetChatdetails();
         return Ok();
      }
    }
}
