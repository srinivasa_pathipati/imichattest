﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Configuration;
using TokenBasedAuth.Models.Interfaces;
using TokenBasedAuth.Models;

namespace TokenBasedAuth.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class PartnerController : ControllerBase
    {
      private readonly IConfiguration _configuration;
     
      private readonly PartnerInfo _partnerfo;
     public PartnerController(IConfiguration configuration,IPartnerRepository partnerRepository)   
     {
       _configuration=configuration;      
       _partnerfo=new PartnerInfo(partnerRepository);
     }
    
      [HttpGet("GetPartners")]
      public IActionResult GetPartnerDetails()
      {
       var result= _partnerfo.GetPartnerDetails();
         return Ok(result);
      }
       [HttpGet("Getpartner/{partnerid}")]
      public IActionResult GetPartnerDetails(int partnerid)
      {
       var result= _partnerfo.GetPartnerDetailsById(partnerid);
         return Ok(result);
      }
    }
}
