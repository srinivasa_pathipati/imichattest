﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Tokens;
using TokenBasedAuth.Helper;

namespace TokenBasedAuth.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OAuthController : ControllerBase
    {

        [HttpGet("token")]
      public IActionResult Token(string authocode)
      {
          var claims=new[]
          {
            new Claim("userid","123"),
           

          };
          var AuthHeader= Request.Headers["Authorization"].ToString();
        //The Following clientscretbytes value coming from input
         var clientscretbytes=Encoding.UTF8.GetBytes(AuthHeader);
         var key =new  SymmetricSecurityKey(clientscretbytes); 
         var alogorithm= SecurityAlgorithms.HmacSha256;
        
         var sigingCredentials=new  SigningCredentials(key,alogorithm);
         var token=new JwtSecurityToken(
             issuer:Constants.Issuer,
             audience: Constants.Audience,
             claims,
             notBefore:DateTime.Now,            
             expires:DateTime.Now.AddHours(1),
             sigingCredentials
         );
         var tokenjson=new JwtSecurityTokenHandler().WriteToken(token);
          return Ok(new{access_token=tokenjson});
      }
    }
}
