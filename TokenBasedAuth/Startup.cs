using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using TokenBasedAuth.Data;
using TokenBasedAuth.Data.Repositories;
using TokenBasedAuth.Models.Interfaces;

namespace TokenBasedAuth
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            _config = configuration;
        }

        public IConfiguration _config { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
         services.AddControllers();
            
         #region  Tokenbased Authentication
            var clientscretbytes=Encoding.UTF8.GetBytes(_config["SigningKey"]);
            var key =new  SymmetricSecurityKey(clientscretbytes); 
            services.AddAuthentication("Oauth")
                .AddJwtBearer("Oauth", config => 
                {
                        config.TokenValidationParameters=new TokenValidationParameters()
                        {
                            ValidAudience= "http://localhost:5000/",
                            ValidIssuer="testapp",
                            IssuerSigningKey=key,
                            
                            
                        };
                });
            #endregion
         services.AddDbContextPool<ApiDBContext>(
            options => options.UseSqlServer(_config.GetConnectionString("ChatDBConnection")));
         services.AddScoped<IChatRepository,ChatRepository>();
          services.AddScoped<IPartnerRepository,PartnerRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
