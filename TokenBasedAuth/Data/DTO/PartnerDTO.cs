namespace TokenBasedAuth.Data.DTO
{
    public class PartnerDTO
    {
        public int id{get;set;}
        public string name{get;set;}
    }
}