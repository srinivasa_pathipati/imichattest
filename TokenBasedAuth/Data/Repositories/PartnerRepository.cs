using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using TokenBasedAuth.Data.DTO;
using TokenBasedAuth.Models.Interfaces;

namespace TokenBasedAuth.Data.Repositories
{
    public class PartnerRepository : IPartnerRepository
    {
     private readonly ApiDBContext context;    
    public PartnerRepository(ApiDBContext context)
    {
        this.context = context;
    }
    
    public List<PartnerDTO>  GetPartnerDetails()
    {
       var queryResult = context.Set<PartnerDTO>().FromSqlInterpolated($"select * from tbl_chat_partner").AsQueryable();
        return queryResult.ToList();
    }
       public PartnerDTO GetPartnerDetailsById(int partnerId)
       {          
           var queryResult=context.PartnerInfo.FromSqlRaw($"getparnterbyId {partnerId}").AsEnumerable().FirstOrDefault();
           return queryResult;
       }
    }   
}
