using Microsoft.EntityFrameworkCore;
using TokenBasedAuth.Data.DTO;

namespace TokenBasedAuth.Data
{
   public class ApiDBContext : DbContext
{
    public ApiDBContext(DbContextOptions<ApiDBContext> options)
        : base(options)
    {
    }
    
        public DbSet<PartnerDTO> PartnerInfo{ get; set; }

}
}